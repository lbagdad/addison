package com.addison.codechallenge.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import java.util.List;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {

    public static final String DAY_SELECTOR = ".c2-day";

    @FindBy(css = "li[data-id='currency_selector']")
    private WebElement currencyIcon;

    @FindBy(id = "currency_dropdown_all")
    private WebElement currencySelector;

    @FindBy(css = ".js-uc-language")
    private WebElement languageIcon;

    @FindBy(id = "current_language_foldout")
    private WebElement languageSelector;

    @FindBy(id = "ss")
    private List<WebElement> destinationField;

    @FindBy(css = ".sb-autocomplete__list")
    private List<WebElement> destinationOptionsList;

    @FindBy(css = ".c2-month")
    private List<WebElement> months;

    @FindBy(css = ".c2-calendar-body")
    private List<WebElement> calendars;

    @FindBy(css = "div[data-calendar2-type='checkout']")
    private WebElement checkoutMonth;

    @FindBy(css = "select[name='group_adults']")
    private WebElement adultsDropDown;

    @FindBy(css = "select[name='group_children']")
    private WebElement childrenDropDown;

    @FindBy(css = "select[name='age']")
    private WebElement ageDropDown;

    @FindBy(css = ".sb-searchbox__button")
    private List<WebElement> searchButton;

    public void selectCurrency(String currency) {
        currencyIcon.click();
        WebElement selectedCurrency = currencySelector.findElement(By.cssSelector("a[data-currency='" + currency + "']"));
        selectedCurrency.click();
    }

    public void selectLanguage(String language) {
        languageIcon.click();
        WebElement selectedLanguage = languageSelector.findElement(By.cssSelector("a[hreflang='" + language + "']"));
        selectedLanguage.click();
    }

    public void setDestination(String destination) {
        destinationField.get(0).click();
        destinationField.get(0).sendKeys(destination);
    }

    public WebElement destinationOptionsList() {
        return destinationOptionsList.get(0);
    }

    public void chooseDestination(WebElement destinationList, String destination) {
        WebElement malagaOption = destinationList.findElement(By.cssSelector("li[data-label='" + destination + "']"));
        malagaOption.click();
    }

    public void checkinLastDayOfCurrentMonth() {
        chooseLastDayOfMonth(months.get(0));
    }
    public void checkoutLastDayOfNextMonth() {
        checkoutMonth.click();
        WebElement checkoutCalendar = calendars.get(1);
        List<WebElement> checkoutMonths = checkoutCalendar.findElements(By.cssSelector(".c2-month"));
        chooseFirstDayOfMonth(checkoutMonths.get(1));
    }


    public void selectAdults(String adults) {
        new Select(adultsDropDown).selectByValue(adults);
    }

    public void selectChildren(String children, String age) {
        new Select(childrenDropDown).selectByValue(children);
        new Select(ageDropDown).selectByValue(age);
    }

    public void clickSearchButton() {
        searchButton.get(0).click();
    }

    private void chooseLastDayOfMonth(WebElement month) {
        List<WebElement> days = daysInMonth(month);
        WebElement lastDay = days.get(days.size() - 1);
        lastDay.click();
    }

    private void chooseFirstDayOfMonth(WebElement month) {
        List<WebElement> days = daysInMonth(month);
        WebElement lastDay = days.get(0);
        lastDay.click();
    }

    private List<WebElement> daysInMonth(WebElement month) {
        return month.findElements(By.cssSelector(DAY_SELECTOR));
    }

}