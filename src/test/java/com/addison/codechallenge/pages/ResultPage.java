package com.addison.codechallenge.pages;

import com.addison.codechallenge.utils.PageUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class ResultPage {

    public static final String SCORING_SELECTOR = ".sr-hotel__name";
    public static final String SCORE_BADGE_SELECTOR = ".review-score-badge";
    public static final String TOTAL_PRICE_SELECTOR = ".totalPrice";
    public static final String EURO_SYMBOL = "€";

    @FindBy(css = "select[name='no_rooms']")
    private WebElement roomsDropDown;

    @FindBy(css = "input[name='sb_travel_purpose']")
    private WebElement travelCheckBox;

    @FindBy(css = ".sr_property_block")
    private List<WebElement> properties;

    public void selectRooms(String rooms) {
        new Select(roomsDropDown).selectByValue(rooms);
    }

    public void clickTravel() {
        travelCheckBox.click();
    }

    public void findFirstProperty(Double minScoring, Double maxPrice) {
        Boolean offerFound = false;
        for (WebElement property : properties) {
            Double propertyScoring = getPropertyScoring(property);
            Double propertyPrice = getPropertyPrice(property);
            offerFound = propertyScoring > minScoring && propertyPrice < maxPrice;
            if (offerFound) {
                String propertyName = getPropertyName(property);
                System.out.println("Found property with name " + propertyName);
                System.out.println("Price " + propertyPrice);
                System.out.println("Scoring " + propertyScoring);
                break;
            }
        }
        Assert.assertTrue("Offer was not found", offerFound);
    }

    private String getPropertyName(WebElement property) {
        String propertyName = null;
        try {
            WebElement nameElement = property.findElement(By.cssSelector(SCORING_SELECTOR));
            propertyName = nameElement.getText();
        } catch (NoSuchElementException e) {
            // Silent exception. Scoring is an optional element.
        }
        return propertyName;
    }

    private Double getPropertyScoring(WebElement property) {
        return PageUtils.parseDouble(getPropertyInfo(property, SCORE_BADGE_SELECTOR));
    }

    private Double getPropertyPrice(WebElement property) {
        Double price = Double.NaN;
        String propertyPrice = getPropertyInfo(property, TOTAL_PRICE_SELECTOR);
        if (propertyPrice != null) {
            price = PageUtils.parseDouble(propertyPrice.split(EURO_SYMBOL)[1]);
        }
        return price;
    }

    private String getPropertyInfo(WebElement property, String cssSelector) {
        String value = null;
        try {
            value = property.findElement(By.cssSelector(cssSelector)).getText();
        } catch (NoSuchElementException e) {
            // Silent exception. Scoring is an optional element.
        }
        return value;
    }
}