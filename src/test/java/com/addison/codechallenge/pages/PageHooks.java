package com.addison.codechallenge.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class PageHooks {

    public static WebDriver setUpDriver() {
        System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--incognito");
        WebDriver driver = new ChromeDriver(chromeOptions);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        return driver;
    }

    public static HomePage setupHomePage(WebDriver driver) {
        HomePage homePage = new HomePage();
        PageFactory.initElements(driver, homePage);
        return homePage;
    }

    public static ResultPage setupResultPage(WebDriver driver) {
        ResultPage resultPage = new ResultPage();
        PageFactory.initElements(driver, resultPage);
        return resultPage;
    }

    public static void tearDown(WebDriver driver) {
        driver.quit();
    }
}
