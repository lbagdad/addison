package com.addison.codechallenge.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    format = { "pretty", "html:target/cucumber" },
    glue = "com.addison.codechallenge.steps",
    features = "classpath:cucumber"
)
public class BookingRunnerTest {

}
