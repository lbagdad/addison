package com.addison.codechallenge.steps;

import com.addison.codechallenge.pages.HomePage;
import com.addison.codechallenge.pages.PageHooks;
import com.addison.codechallenge.pages.ResultPage;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BookingSteps {

    WebDriver driver;
    HomePage homePage;
    ResultPage resultPage;

    @Before
    public void setUp() {
        driver = PageHooks.setUpDriver();
        homePage = PageHooks.setupHomePage(driver);
        resultPage = PageHooks.setupResultPage(driver);
    }

    @Given("I navigate to booking.com website")
    public void openBookingPage() {
        driver.get("https://www.booking.com/");
        driver.manage().window().maximize();
    }

    @When("I choose (.*) currency and (.*) language")
    public void chooseCurrencyAndLanguage(String currency, String language) {
        homePage.selectCurrency(currency);
        homePage.selectLanguage(language);
    }

    @When("I set (.*) in destination field")
    public void setDestinationField(String destination) {
        homePage.setDestination(destination);
    }

    @When("I select (.*) destination option")
    public void chooseDestinationOption(String destination) {
        WebElement destinationList = homePage.destinationOptionsList();
        homePage.chooseDestination(destinationList, destination);
    }

    @When("I check-in last day of current month")
    public void checkin() {
        homePage.checkinLastDayOfCurrentMonth();
    }

    @When("I check-out first day of next month")
    public void checkout() {
        homePage.checkoutLastDayOfNextMonth();
    }

    @When("I select (.*) adults")
    public void selectAdults(String adults) {
        homePage.selectAdults(adults);
    }

    @When("I select (.*) children (.*) years old")
    public void selectChildren(String children, String age) {
        homePage.selectChildren(children, age);
    }

    @When("I click on search button")
    public void clickSearchButton() {
        homePage.clickSearchButton();
    }

    @When("I select (.*) rooms")
    public void selectRooms(String rooms) {
        resultPage.selectRooms(rooms);
    }

    @When("I click on I'm traveling for work checkbox")
    public void clickTravelCheckBox() {
        resultPage.clickTravel();
    }

    @Then("I find a property with scoring higher than (.*) and price under (.*) EUR")
    public void checkProperty(Double scoring, Double price) {
        resultPage.findFirstProperty(scoring, price);
    }

    @After
    public void quit() {
        PageHooks.tearDown(driver);
    }
}
