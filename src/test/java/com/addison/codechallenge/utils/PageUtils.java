package com.addison.codechallenge.utils;

public class PageUtils {

    public static Double parseDouble(String optional) {
        return optional != null ? Double.parseDouble(optional) : Double.NaN;
    }
}