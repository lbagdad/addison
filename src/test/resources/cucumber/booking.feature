Feature: Booking search
  As a user
  I want to search for a trip to Málaga

  Scenario: Search hotel in Málaga
    Given I navigate to booking.com website
    When I choose EUR currency and en-us language
    And I set Málaga, Andalucía in destination field
    And I select Málaga, Andalucía, Spain destination option
    And I check-in last day of current month
    And I check-out first day of next month
    And I select 1 adults
    And I select 1 children 5 years old
    And I click on search button
    And I select 2 rooms
    And I click on I'm traveling for work checkbox
    And I click on search button
    Then I find a property with scoring higher than 8.0 and price under 200 EUR