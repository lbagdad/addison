# Booking acceptance test

Proof of concept project for testing Booking.com test page.
Testing based on the classic page of booking.com

## Technologies used

- Cucumber
- Selenium
- Java

### Prerequisites

- JDK 7
- Maven 3
- Google chrome
- Google chrome driver

## Running the tests

Run from the shell

```
mvn clean test
``` 
## Improvements

- Support for different browsers (Safari, Firefox, etc).
- Using hooks for driver initialisation and tear down.
- Execution on headless mode for supported browsers.
- Support for new version of booking.com home page.
 
## Author

* **Loubna Bagdad** - lbagdad@me.com
